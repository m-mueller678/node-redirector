"use strict";

const fs = require('fs');
const path = require('path');
const express = require('express')
const validator = require('validator')

const app = express()
const port = process.env.PORT
const client_limit=10
const app_dir = path.dirname(require.main.filename)

var client_data={}

function Client(ip,req){
		this.target_url=''
		this.update_info(ip,req)
}

Client.prototype.update_info=function(ip,req){
	this.time=Date()
	this.ip=''+ip
	this.user_agent=req.headers['user-agent']
}

Client.prototype.relevance=function(){
	r=this.time/1000//seconds since epoch
	if(this.target_url !== ''){
			r=r+24*3600
	}
	return r
}

function drop_least_relevant_client(){
	min=Object.keys(client_data)[0]
	for(var client_key in client_data){
		if(client_data[client_key].relevance()<client_data[min].relevance()){
				min=client_key
		}
	}
	delete client_data[min]
}

function add_or_update_client(ip,req){
	if(typeof client_data[ip] === 'undefined'){
		client_data[ip]=new Client(ip,req)
		if(Object.keys(client_data).length>client_limit){
			drop_least_relevant_client()
		}
	}else{
		client_data[ip].update_info(ip,req)
	}
	return client_data[ip]
}

function redirect(req,res,phase){
	var ip=req.headers['x-forwarded-for'] || req.connection.remoteAddress
	ip=ip.toString()
	var client=add_or_update_client(ip,req)
	if(client.target_url === ''){
		res.sendFile(app_dir+'/landing.html')
	}else if(phase===0){
		res.sendFile(app_dir+'/redirect.html')
	}else{
		res.redirect(client.target_url)
	}
}

app.use(express.urlencoded({ extended: true ,limit:10000}))
app.get('/', (req, res) =>redirect(req,res,0))
app.get('/redirect',(req,res)=>redirect(req,res,1))
app.get('/control',(req,res)=>res.sendFile(app_dir+'/control.html'))
app.get('/clients',(req,res)=>res.json(client_data))

app.post('/set_target',(req,res)=>{
	if(typeof req.body.target !=='string'
	   || typeof req.body.client !== 'string'
	   || typeof client_data[req.body.client] ==='undefined'
	   || ( !validator.isURL(req.body.target) && req.body.target !== '')
	){
		throw new Error("bad POST data")
	}
	client_data[req.body.client].target_url=req.body.target
	res.redirect(303,'control')
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
